package co.tecnofactor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.tecnofactor.model.Employee;

/**
 * 
 * @author farid.rico
 *
 */

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	/**
	 * Consulta los empleados por id
	 * 
	 * @param id indentificador unico en la db
	 */
	public Employee findById(int id);
	
	/**
	 * Actualiza cada uno de los atributos del empleado
	 * 
	 * @param name Nombre del empleado
	 * @param lastname Apellidos del usuario
	 * @param age Edad del empleado
	 * @param id Identificador de empleado en la db
	 * */
	@Modifying
	@Query("UPDATE Employee s SET s.name = ?1, s.lastname = ?2, s.age = ?3 WHERE s.id = ?4")
	public int setEmployee(String name, String lastname, int age, int id);

}
