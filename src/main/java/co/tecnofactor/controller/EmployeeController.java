package co.tecnofactor.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.tecnofactor.model.Employee;
import co.tecnofactor.repository.EmployeeRepository;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	private EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeController(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public EmployeeController() {}

	@GetMapping("/employees")
	public List<Employee> getAll() {
		if (!this.employeeRepository.findAll().isEmpty())
			return this.employeeRepository.findAll();
		else
			throw new EmployeeNotFoundException();
	}

	@GetMapping("/employee/{id}")
	public Employee get(@PathVariable int id) {

		if (this.employeeRepository.findById(id) != null)
			return this.employeeRepository.findById(id);
		else
			throw new EmployeeNotFoundException(id);
	}

	@PostMapping("/employee/add")
	@Transactional
	public Employee add(@RequestBody Employee employee) {
		return this.employeeRepository
				.save(employee);
	}

	@PutMapping("/employee/modify")
	@Transactional
	public int modify(@RequestBody Employee employee) {
		return this.employeeRepository.setEmployee(employee.getName(), employee.getLastname(), employee.getAge(),
				employee.getIdEmployee());
	}

	@DeleteMapping("/employee/delete/{id}")
	public void delete(@PathVariable int id) {
		this.employeeRepository.deleteById(id);
	}
}
