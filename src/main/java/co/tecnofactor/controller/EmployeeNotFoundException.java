package co.tecnofactor.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class EmployeeNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmployeeNotFoundException() {
		super("No se encontro ning�n empleado");
	}
	
	public EmployeeNotFoundException(int id) {
		super(String.format("El empleado con ID %d no existe", id));
	}
}