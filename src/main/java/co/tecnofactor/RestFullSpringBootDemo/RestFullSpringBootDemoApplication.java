package co.tecnofactor.RestFullSpringBootDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("co.tecnofactor.controller")
@EntityScan("co.tecnofactor.model")
@EnableJpaRepositories("co.tecnofactor.repository")
public class RestFullSpringBootDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestFullSpringBootDemoApplication.class, args);
	}
}
